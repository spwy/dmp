package cn.edu360.utils

import java.util.Properties
import com.typesafe.config.{Config, ConfigFactory}


object ConfigHandler {
  private lazy val config: Config = ConfigFactory.load()

  // parquet文件所在的路径
  val parquetPath = config.getString("parquet.path")
  val logdataAnalysisResultJsonPath = config.getString("rpt.logDataAnalysis")

  // 关于MySQL配置
  val url: String = config.getString("db.url")
  val driver: String = config.getString("db.driver")
  val user: String = config.getString("db.user")
  val password: String = config.getString("db.password")
  val logDataAnalysisTableName = config.getString("db.logDataAnalysis.table")
  val areaAnalysisTableName = config.getString("db.areaAnalysis.table")
  val mediaAnalysisTableName = config.getString("db.mediaAnalysis.table")
  val operatorAnalysisTableName = config.getString("db.operatorAnalysis.table")
  val netTypeAnalysisTableName = config.getString("db.netTypeAnalysis.table")
  val deviceTypeAnalysisTableName = config.getString("db.deviceTypeAnalysis.table")
  val operateSystemAnalysisTableName = config.getString("db.operateSystemAnalysis.table")
  val appNameAnalysisTableName = config.getString("db.appNameAnalysis.table")


  //封装MySQL Props
  val props = new Properties()
  props.setProperty("driver", driver)
  props.setProperty("user", user)
  props.setProperty("password", password)

  val appDictPath = config.getString("appdict")

  //redis配置
  val redisHost = config.getString("redis.host")
  val redisPort = config.getInt("redis.port")
  val redisIndex = config.getInt("redis.index")
}
