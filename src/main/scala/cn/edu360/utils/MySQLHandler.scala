package cn.edu360.utils

import org.apache.spark.sql.{DataFrame, SaveMode}

object MySQLHandler {

  def save2db(resultDF:DataFrame,tblName:String,partiton:Int =1) ={
    resultDF.coalesce(partiton).write.mode(SaveMode.Overwrite).jdbc(
      ConfigHandler.url,
      tblName,
      ConfigHandler.props
    )
  }
}
