package cn.edu360.utils

import redis.clients.jedis.{Jedis, JedisPool}

object Jpools {

  private lazy val jedisPool = new JedisPool(ConfigHandler.redisHost,ConfigHandler.redisPort)

  def getJedis: Jedis ={
    val jedis = jedisPool.getResource
    jedis.select(ConfigHandler.redisIndex)
    jedis
  }
}
