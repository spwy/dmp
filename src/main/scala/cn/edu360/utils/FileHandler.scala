package cn.edu360.utils

import org.apache.hadoop.fs.{FileSystem, Path}
import org.apache.spark.SparkContext

object FileHandler {

  def deleteWillOutputDir(sc: SparkContext, outputPath: String) = {
    val fs = FileSystem.get(sc.hadoopConfiguration)
    val path = new Path(outputPath)
    if (fs.exists(path)) {
      fs.delete(path, true)
    }
  }
}
