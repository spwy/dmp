package cn.edu360.beans

case class ReportLogDataAnalysis(ct:Int,provinceName:String,cityName:String)

case class ReportAreaAnalysis(provinceName:String,
                              cityName:String,
                              rawReq:Double,
                              effReq:Double,
                              adReq:Double,
                              rtbReq:Double,
                              winReq:Double,
                              adShow:Double,
                              adClick:Double,
                              adCost:Double,
                              adPayment:Double
                             )
case class ReportMediaAnalysis(appName:String,
                               rawReq:Double,
                               effReq:Double,
                               adReq:Double,
                               rtbReq:Double,
                               winReq:Double,
                               adShow:Double,
                               adClick:Double,
                               adCost:Double,
                               adPayment:Double
                              )
